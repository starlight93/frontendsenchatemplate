var findParentModel = function(el) {
	var parent = el.getParent();
	if (parent.getViewModel() != undefined && parent.getViewModel() != null ) {
        return parent.getViewModel();
    } else {
        return findParentModel(parent);
    }
};
var keyPress =(e)=>{
    if(e.key === "Escape") {
        if(confirm('refresh anda setelah ini akan ke home?')){
            sessionStorage.clear();
            window.location.replace("/");
        }
    }
}