function globalStores(){
    
    Ext.create({
        xtype:'dialog',
        maximizable: false,
        draggable:false,
        title:"Loading Bill Of Materials....",
        // closable:false,
        id: "loadingDialogBOM",
        shadow:false,
        modal:true,
        style:{
            background:'transparent'
        },
        cls:"customLoader",
        width:300,
        viewModel:{
            progress:0.0
        },
        html: `<div class="loader"></div>`,
        listeners:{
            beforeshow:function(){
                // console.log('show');
            }
        }
    });
    Ext.create({
        xtype:'dialog',
        maximizable: false,
        draggable:false,
        title:"Generating Report....",
        // closable:false,
        id: "loadingDialogReport",
        shadow:false,
        modal:true,
        style:{
            background:'transparent'
        },
        cls:"customLoader",
        width:300,
        viewModel:{
            progress:0.0
        },
        html: `<div class="loader"></div>`,
        listeners:{
            beforeshow:function(){
                // console.log('show');
            }
        }
    });
    Ext.create({
        xtype:'dialog',
        maximizable: false,
        // closable:false,
        id: "loadingDialog",
        shadow:false,
        modal:true,
        style:{
            background:'transparent'
        },
        cls:"customLoader",
        width:300,
        viewModel:{
            progress:0.0
        },
        html: `<div class="loader"></div>`,
        listeners:{
            beforeshow:function(){
                // console.log('show');
            }
        }
    });
    Ext.create({
        xtype:'dialog',
        title:false,
        maximizable: false,
        closable:false,
        id: "QLReportPopupV2",
        shadow:true,
        modal:true,
        minWidth:950,
        minHeight:500,
        showData:function(data){
            this.getItems().items[0].pdfRefresh(data);
            this.show();
        },        
        buttons: {
            close: function(){
                this.up("dialog").hide();
            },
        },
        items:[{
            xtype: "QLPDFReportV2",
            height:490,
            withButtons:false
       }]
    });
}