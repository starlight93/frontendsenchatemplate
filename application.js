Ext.define('qlproject.Application', {
	extend: 'Ext.app.Application',
	name: 'qlproject',
	requires: ['qlproject.*'],
	defaultToken:"Login",
	removeSplash: function () {
		Ext.getBody().removeCls('launching')
		var elem = document.getElementById("splash")
		elem.parentNode.removeChild(elem)
	},
	launch: function () {
		document.addEventListener('keydown',keyPress);
		globalStores();
		if(localStorage.auth!==undefined){
			api.setHeaders(JSON.parse(localStorage.auth));
		}	
		this.removeSplash();
		localStorage.first = "1";
		api.getMe();
	},

	onAppUpdate: function () {
		Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
			function (choice) {
				if (choice === 'yes') {
					window.location.reload();
				}
			}
		);
	},

	afterLogin:function(){
		Ext.StoreManager.lookup("storemenulist").setRoot({
			"expanded": true,
			"children": qlconfig_sidebar
		});
	},
	
	loginSuccess:function(JSON){
		//algoritma LOGIN SUKSES MAU NGAPAIN
		console.log(JSON);
		try{
			let auth = {
				Authorization	: `${JSON['token_type']} ${JSON['token']}`
			};
			api.setHeaders(auth);
		}catch(e){}
		this.setDefaultToken("Home");
		if((Ext.Viewport.getItems().items).length>0){
			Ext.Viewport.remove(0);
		}
		Ext.Viewport.add([{xtype:"projectmainview"}]);
		// window.location.href="#Home";		
		var oldXtype = localStorage.xtype!=undefined?localStorage.xtype:"Home";
		window.location.href="#-";
		setTimeout(function () {
			window.location.href="#"+oldXtype;
		},100);
		localStorage.first="0";
	}
});
