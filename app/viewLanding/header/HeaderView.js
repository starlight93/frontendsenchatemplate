Ext.define('project.HeaderView', {
    extend: 'Ext.Toolbar',
    xtype: 'projectheaderview',
	cls: 'headerview',
	shadow: true,
    viewModel: {},
	// layout: {
    //     type: 'vbox'
    // },
    items: [		
        { 
			xtype: 'container',
            cls: 'headerviewtext',
            bind: { html: '{heading}' }
        },
        '->',{
		xtype: 'button',
		ui: 'confirm',
		// style:"background-color:white",
		// text: 'Menu',
		id:"header-btn-maximize",
		tooltip:'Maximize',
		arrow:false,
		iconCls: "x-fas fa-desktop",
		handler:function(el){
			let elem=document.body;
			if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
				
				el.setTooltip("Minimize");
				el.setIconCls("fas fa-angle-double-down");
				if (elem.requestFullScreen) {
					elem.requestFullScreen();
				} else if (elem.mozRequestFullScreen) {
					elem.mozRequestFullScreen();
				} else if (elem.webkitRequestFullScreen) {
					elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
				} else if (elem.msRequestFullscreen) {
					elem.msRequestFullscreen();
				}
			} else {
				el.setTooltip("Maximize");
				el.setIconCls("x-fas fa-desktop");
				if (document.cancelFullScreen) {
					document.cancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}
			}
		}
	},{
		xtype: 'button',
		alignSelf: 'right',
		tooltip:'Profile',
		// ui: 'plain',
		style:"background-color:white",
		// text: 'Menu',
		arrow:true,
		id:"header-btn-profile",
		iconCls: 'x-fa fa-user',
		menu: {
			anchor: true,
			items: [
				{
					xtype:'container',
					layout:'hbox',
					style:{
						margin:'auto',
					},
					items:[
						{
							xtype:'image',
							src:'resources/images/developer.png',
							height: 80,
							width: 80,
							style:{
								padding:10
							}
						},
						{
							xtype: 'displayfield',
							afterRender:function(){
								if(localStorage.user_information!==undefined){
									this.setValue(JSON.parse(localStorage.user_information)["name"]);
								}
							}
						}

					]
				}
				, '-', {
					xtype: 'button',
					text:'Logout',
					ui:'alt decline',
					// align:"right",
					margin:'auto',
					handler: function(){                        
                        localStorage.clear();
                        window.location.href="/";
                    }
				}
			]
		}
	}]
});
