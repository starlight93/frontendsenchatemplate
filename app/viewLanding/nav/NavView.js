Ext.define('project.NavView', {
    extend: 'Ext.Panel',
    xtype: 'projectnavview',
    controller: "projectnavviewcontroller",
    cls: 'navview',
    viewModel: {},
    layout: 'fit',
    tbar: {xtype: 'projecttopview', height: 50},
    items: [ 
        {
            xtype: 'projectmenuview', 
            reference: 'projectmenuview', 
            bind: {width: '{menuview_width}'}, 
            listeners: { 
                selectionchange: "onMenuViewSelectionChange"
            }
        }
    ],
    bbar: {xtype: 'bottomview', bind: {height: '{bottomview_height}'}}
});
