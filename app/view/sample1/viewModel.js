Ext.define('qlproject.view.sample1.viewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.sample1-view',
    data: {
        name: 'qlproject',
        isUpdating : false, //UNTUK KEPERLUAN UPDATE
        resource_id : null //UNTUK KEPERLUAN UPDATE
    },
    stores:{
        sample1detail:{    //UNTUK KEPERLUAN GRID DETAIL BAWAH
            storeId : "sample1detail",
            data    : []
        }
    }

});
