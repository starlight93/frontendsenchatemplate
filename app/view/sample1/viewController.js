Ext.define('qlproject.view.sample1.viewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sample1-view',
    rowEdit:function(el,row){
        var viewModel = this.getViewModel(); //mengakses viewModel saat ini untuk persiapan mengubah value data isUpdating dan resource_id
        api.read({
            model: "sample1", 
            id :row.record.id
        }, function(json){
            el.up("navigationview").getItems().items[0].getItems().items[0]._handler(); //jurus tercepat transisi navigation ke form
            viewModel.set("isUpdating",true);
            viewModel.set("resource_id",json.id);
            Ext.getCmp("sample1").setValues(json); // ISI DATA FIELD-FIELD di FORM HEADER dulu dg getCmp id form header
            Ext.StoreManager.lookup('sample1detail').setData(json["sample1_detail"]); // json['sample1_detail] sesuai output api untuk diisikan ke store detail
        }); 
    },
    saveOrUpdate : function(el){
        var formHeader = Ext.getCmp("sample1"); //akses FORM dari ID form header
        var dataSiapPost = formHeader.getValues(); // dapatkan isi field-field form header
        var storeSample1Detail = Ext.StoreManager.lookup('sample1detail'); //ambil dari ID Store  grid di detail form
        var dataSample1Detail = [] //siapkan penampung array detail
        let arrayDetail = storeSample1Detail.getData().items;
        arrayDetail.forEach(function(dt) {
            dataSample1Detail.push(dt.data); //masukkan data detail ke array data detail
        });
        dataSiapPost['sample1_detail'] = dataSample1Detail; //satukan data array detail dengan data form Header
        //==================================================UPDATING REQ
        var viewModel = this.getViewModel();
        if(viewModel.get("isUpdating")){ //JIKA VIEWMODEL data isUpdating (karena fungsi rowEdit di atas) sedang true, maka dalam keadaan update aktivitasnya
            api.update(
                {
                    model   : "sample1",
                    id      : viewModel.get("resource_id"),
                    data    : dataSiapPost
                }, 
                function(json)
                { //PASCA TERUPDATE SUKSES
                    Ext.Msg.alert('Success', 'Data has been updated successfully', Ext.emptyFn);
                    formHeader.reset(true); //Kosongkan lagi Formheader    
                    storeSample1Detail.removeAll(); //kosongkan data store detail
                    el.up("navigationview").pop(); // kembali ke table list halaman depan
                    Ext.StoreManager.lookup("sample1").load(); //load datatable halaman depan agar refresh
                }
            )        
        //================================= dibawah ini jika isUpdating FALSE, atau tidak dalam aktivitas update, maka create
        }else{
            api.create(
                {
                    model   : "sample1",
                    data    : dataSiapPost
                }, 
                function(json)
                { //PASCA TERSIMPAN SUKSES
                    Ext.Msg.alert('Success', 'Data has been submitted successfully', Ext.emptyFn);
                    formHeader.reset(true); //Kosongkan lagi Formheader   
                    storeSample1Detail.removeAll(); //kosongkan data store detail
                    el.up("navigationview").pop();  // kembali ke table list halaman depan
                    Ext.StoreManager.lookup("sample1").load(); //load datatable halaman depan agar refresh
    
                }
            )
        }
    },
    cancel : function(el){
        Ext.getCmp("__GANTI_ID_FORM_HEADER").reset(true); //MERESET FORM HEADER AGAR KOSONG
        // Ext.getCmp("__GANTI_ID_FORM_DETAIL").reset(true);//MERESET FORM DETAIL AGAR KOSONG JIKA ADA
        // Ext.StoreManager.lookup("__GANTI_ID_STORE_DETAIL").removeAll(); //MENGOSONGKAN GRID DETAIL JIKA ADA
        el.up("navigationview").pop();
    },
    sample1_detail_addToList : function(el){
        var dataDetailForm = el.up("formpanel").getValues(); // DAPATKAN VALUES DATA FORM DETAIL
        var detailStore = el.up("tabpanel").down("grid").getStore(); // AKSES STORE GRID BAWAHNYA FORM
        detailStore.add(dataDetailForm); // MASUKKAN DATA FORM DETAIL KE STORE
        el.up("formpanel").reset(true); // CLEAR FORM DETAIL
    },
    sample1_detail_clearForm : function(el){
        el.up("formpanel").reset(true); // CLEAR FORM DETAIL
    },


});
