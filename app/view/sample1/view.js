// TUTORIAL taruh JSON data dari laradev ke variable rawData
var rawData = {
    "json_version": 2,
    "process": "Header",
    "forms": [
        {
            "name": "SampleForm1",
            "type": "Header",
            "data": [
                {
                    "id": "draft_no",
                    "label-name-type": "Draft No.*-*draft_no*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "item_type",
                    "label-name-type": "Item Type*-*item_type*-*search",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": "",
                    "default": null
                },
                {
                    "id": "header_note",
                    "label-name-type": "Header Note*-*header_note*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "nilai",
                    "label-name-type": "Nilai Angka*-*nilai*-*number",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "status",
                    "label-name-type": "Status*-*status*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*true*-*false*-*false",
                    "value": null,
                    "default": null
                }
            ]
        },
        {
            "name": "Sample1",
            "type": "Detail",
            "data": [
                {
                    "id": "nama",
                    "label-name-type": "Nama*-*nama*-*popup",
                    "ch-req-ro-dis-hd": "false*-*true*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "gender",
                    "label-name-type": "Gender*-*gender*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": "",
                    "default": null
                },
                {
                    "id": "kelas",
                    "label-name-type": "Kelas*-*kelas*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": "",
                    "default": null
                },
                {
                    "id": "umur",
                    "label-name-type": "Umur*-*umur*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                },
                {
                    "id": "hobi",
                    "label-name-type": "Hobi*-*hobi*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": "",
                    "default": null
                },
                {
                    "id": "detail_note",
                    "label-name-type": "Detail Note*-*detail_note*-*text",
                    "ch-req-ro-dis-hd": "false*-*false*-*false*-*false*-*false",
                    "value": null,
                    "default": null
                }
            ]
        }
    ]
}
// TUTORIAL panggil decoder sesuai indexJSON di rawData dan taruh di variable misal sample1 dan sample1_detail
var sample1 = new QL.DecoderV5({
   data            : rawData,
   type            : "header", //header atau detail
   id              : "sample1",
   indexJSON       : 0, //TUTORIAL PERTHATIKAN INI SESUAI INDEX FORM DI rawData
   cols            : 2,
   defaultWidth    : "100%",
   labelWidth      : "100%",
   rowSpaces       : [{
        id       : "draft_no", 
        position : "after",
        total    : 1
    }],
    properties     :[{
        id : "item_type",
        props : {            
            store   :"store_online_model_contoh", // TUTORIAL LIHAT di folder app/GlobalStores cari nama ini dapat dari mana
            valueField      : "id",
            displayField    : "nama",
        }
    }]
});

var sample1_detail = new QL.DecoderV5({
   data            : rawData,
   type            : "detail",         //TUTORIAL header atau detail
   id              : "sample1_detail",
   indexJSON       : 1, 
   cols            : 2,
   defaultWidth    : "100%",
   labelWidth      : "90%",
   rowSpaces       : [],
   properties      : [{
       id : 'grade',
       props: {
           required : true
       }
   },
   {
       id : "nama", // TUTORIAL contoh MEMANGGIL POPUP DATATABLE
       props: {
           popupbutton:{
                    handler:function(button){
                        Ext.create({
                            xtype:"QLDatatablePopupV1",
                            title:"Daftar Nama Dicari Polisi",
                            gridHeight:"400px",
                            store:{
                                proxy:{
                                    model : "sample1", 
                                    parameters:{
                                        paginate:25
                                    },
                                }
                            },
                            columns: [     
                                {text:"No",xtype:"rownumberer",align: "center",width:"10%"},
                                {text:"Nama",dataIndex:"nama",width:"15%"},
                                {text:"Kelas",dataIndex:"kelas",width:"20%"},
                                {text:"Umur",dataIndex:"umur",width:"15%"},
                                {text:"Hobi",dataIndex:"hobi",width:"15%"},
                                {text:"Kota",dataIndex:"kota",width:"15%"},
                                {text : 'Action',align: 'center',width : "10%",
                                    cell:{
                                        tools:{
                                            pilih:{
                                                iconCls: "x-fa fa-check",                                        
                                                handler: function(el,row){
                                                    var data = row.record.data; //DATA TERPILIH ADA DI SINI
                                                    // BERIKUT CARA MENGISI KE FIELD FORM DETAIL
                                                    button.up("formpanel").setValues({
                                                        nama : data["nama"], //misal ke field yag punya name 'nama' yakni field ini sendiri
                                                        nama_id : data["id"],  // nama_id adalah field tidak terlihat untuk menyimpan id dari nama
                                                        umur : data["umur"] //misal mengisi field lain selain field popup
                                                    });

                                                    el.up("dialog").destroy();
                                                }
                                            }
                                        }
                                    }
                                }
                            ],  
                        }).show();
                    }
                }
       }
   },]
});

// TUTORIAL gabungkan semua hasil decoder ke panel/tabpanel/form dengan tombol2 
var sample1_panel = {
    xtype  : 'panel',
    scrollable : true, 
    items:[
        {
            xtype:'formpanel',
            title: 'Sample 1 Form',
            items:sample1
        },
        {
            xtype : "tabpanel",
            height: "500px", //SESUAIKAN DENGAN HEIGHT CHILD KALIAN AGAR TIDAK TERPOTONG
            items : [
                {
                    title : "Detail Sample 1", //JUDUL TAB
                    items : [
                        {
                            xtype:'formpanel',
                            items:sample1_detail,
                            buttonAlign:'center',
                            buttons: 
                            [
                                {
                                    text: 'Add to List',
                                    iconCls: "x-fa fa-plus",
                                    ui:'plain',
                                    style: 'background-color: #2dd9e7; color: white;',
                                    handler: 'sample1_detail_addToList' //lihat viewcontroller
                                },
                                {
                                    text: 'Clear',
                                    iconCls: "x-fa fa-eraser",
                                    alignSelf:'center','margin':'5 5 5 5', 
                                    ui:'normal',
                                    handler: 'sample1_detail_clearForm' //lihat viewcontroller
                                }   
                            ]
                        },
                        {
                            xtype: 'grid',
                            height: '200px', //SESUAIKAN INI 
                            scrollable:true,
                            bind: {
                                store: "{sample1detail}"
                            },
                            columns: [
                                {
                                    text: 'No',
                                    xtype: 'rownumberer',
                                    align: 'center',
                                    width : "10%"
                                },
                                {
                                    text: 'Nama',
                                    width: '20%',
                                    dataIndex: 'nama'
                                },
                                {
                                    text: 'Gender',
                                    width: '20%',
                                    dataIndex: 'gender'
                                },
                                {
                                    text: 'Kelas',
                                    width: '20%',
                                    dataIndex: 'kelas'
                                },
                                {
                                    text: 'Umur',
                                    width: '20%',
                                    dataIndex: 'umur'
                                },
                                {
                                    text: 'Action',
                                    width: '10%',
                                    align: 'center',
                                    cell:{
                                        tools:
                                        {
                                            delete : {
                                                iconCls : 'x-fa fa-trash',
                                                handler : function(el,row){
                                                    Ext.Msg.confirm("Confirmation", 'Yakin dihapus?', function (answer) {
                                                        if (answer=='yes') {row.grid.getStore().remove(row.record);}
                                                    })
                                                }
                                            }
                                        }
                                    }
                                }
                            ]
                        }]
                }

            ]
        },
        {
            xtype        :'formpanel',
            buttonAlign  :'center',
            buttons      : [
                {
                    bind:{
                        text : "{isUpdating?'Update':'Save'}"
                    },
                    margin: '5 5 5 5', ui: 'alt confirm', iconCls: "x-fa fa-save", 
                    handler : 'saveOrUpdate' //lihat viewcontroller
                },
                {
                    text:'Cancel' , alignSelf:'center', 
                    margin:'5 5 5 5', iconCls: "x-fa fa-window-close", ui: 'alt decline',
                    handler: 'cancel'  //lihat viewcontroller
                }
            ],
        }
    ]
};

// TUTORIAL lengkapi konfigurasi di atas ke class bawah ini
Ext.define('qlproject.view.sample1.view',{
    extend: 'Ext.panel.Panel',
    xtype : 'sample1',
    requires: [
        'qlproject.view.sample1.viewController',
        'qlproject.view.sample1.viewModel'
    ],
    controller: 'sample1-view',
    viewModel: {
        type: 'sample1-view'
    },
    items: [{
        xtype   : 'navigationview', 
        id      : 'nav_sample1',
        fullscreen: true,
        navigationBar:false,
        items: [{
            items: [{
                xtype: 'button',
                text: 'Create New',
                handler: function(){
                    Ext.getCmp("loadingDialog").show();
                    var nav = this.up("navigationview");
                    nav.push(sample1_panel);
                    Ext.getCmp("loadingDialog").hide();
                }
            },{
                xtype:"QLDatatableV1",
                controller: 'sample1-view', //TAMBAHIN CONTROLLER SESUAI DI CONTROLLER DI ATAS
                gridHeight:"350px",
                style:{
                    "margin-top":"10px"
                },
                store:{
                    proxy:{
                        model : "sample1",
                        parameters:{
                            paginate: 50
                        },
                    }   
                },
                columns:[
                    {
                        text: 'No',
                        xtype: 'rownumberer',
                        align: 'center',
                        width : "10%"
                    },
                    {
                        text : 'Nama',
                        dataIndex: "nama",
                        width : "15%"
                    },
                    {
                        text : 'Kelas',
                        dataIndex: "kelas",
                        width : "15%"
                    },
                    {
                        text : 'Kota',
                        dataIndex: "kelas",
                        width : "15%"
                    },
                    {
                        text : 'Hobi',
                        dataIndex: "hobi",
                        align: 'center',
                        width : "15%"
                    },
                    {
                        text : 'Umur',
                        dataIndex: "umur",
                        align: 'center',
                        width : "20%",
                        renderer:function(val){
                            return val+ "tahun"
                        }
                    },
                    {
                        text    : 'Action',
                        align   : 'center',
                        width   : "10%",
                        cell    : {
                            tools:{
                            // ====================CONTOH EDIT
                                update:{
                                    tooltip:"edit",
                                    iconCls: "x-fa fa-edit",                                        
                                    handler: 'rowEdit' //Lihat ViewController
                                },
                            // ====================ENDING EDIT
                                delete:{
                                    iconCls: "x-fa fa-trash",                                        
                                    handler: 'QLApiDelete' //OTOMATIS TINGGAL TULIS dapat dari Library
                                }
                            //===================================
                            }
                        }
                    }
                ],
            }]
        }]
    }]
});
