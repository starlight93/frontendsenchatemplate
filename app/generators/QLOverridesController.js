
Ext.define('QL.controller',{
    override: 'Ext.app.ViewController',
	QLLoginExecuting: function (el) {
        var form = el.up("formpanel");
        var formData = form.getValues();
        var data = {
            data:formData
        }
        if(qlconfig.use_login){
            api.login(data, function(json){
                qlproject.getApplication().loginSuccess(json);
            },function(error){
                console.log(error);
            });
        }else{
            var json = {
                user : "anonymous"
            }
            qlproject.getApplication().loginSuccess(json);
        }
    },

    QLNavigationBack:function(el){
        el.up("navigationview").pop();
    },

    QLApiDelete:function(el,row){                             
        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete?',
        function(answer) {
            if(answer=="yes"){
                api.delete({
                    model: row.grid.apiModel, 
                    id :row.record.data.id
                }, function(json){
                    row.record.store.load();
                });
                
            }
        });
    },
    QLDetailDelete:function(el,row){
        Ext.Msg.confirm("Confirmation", 'Yakin dihapus?', function (answer) {
            if (answer=='yes') {row.grid.getStore().remove(row.record);}
        })
    },
    QLResetForm:function(el){
        el.up("formpanel").reset(true); // CLEAR FORM DETAIL
    },
	formFocusErrors: function (el) {
        
        var form = (el.up("formpanel") ==null)?form = this.getView():el.up("formpanel");
			// errorCmp = this.lookup('formErrorState'),
			// tooltip = errorCmp.getTooltip(),
		var	errors = [],
			data = {
				errors: errors
			};
            var firstFieldError=null;

		form.getFields(false).forEach(function (field) {
			var error;
			if (!field.validate() && (error = field.getError())) {
				errors.push({
					errors: error,
					name: field.getLabel()
                });
                if(firstFieldError==null){
                    firstFieldError = field;
                }
			}
        });
        if(firstFieldError!=null){
            firstFieldError.focus(false,1200);
            console.log(data);
        }
		// if (errors.length) {
		// 	tooltip.setData(data);
		// 	tooltip.show();
		// } else {
		// 	tooltip.hide();
		// }

		// errorCmp.setData(data);
	},
	formFieldErrors: function () {
        
    },
    
    maxRows:function(dataView){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();
        thisModel.set('currentRows',dataView.value);
        gridStore.getProxy().setExtraParam( parent.configParams["pagePaginate"], dataView.value=='All'?9999:dataView.value);        
        gridStore.getProxy().setExtraParam( parent.configParams["pageNumber"],1);
        gridStore.load();
    },
    pressPage: function(element){
        let parent = element.up("QLDatatableV1");
        var gridStore = parent.down("grid").getStore();
        gridStore.getProxy().setExtraParam(parent.configParams["pageNumber"],element.getText());
        gridStore.load();
    },
    pressNext: function(element){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();

        gridStore.getProxy().setExtraParam(parent.configParams["pageNumber"], 1 + parseInt(thisModel.get("gridCurrentPage")));
        gridStore.load();
    },
    pressPrev: function(element){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();

        gridStore.getProxy().setExtraParam(parent.configParams["pageNumber"], parseInt(thisModel.get("gridCurrentPage")-1));
        gridStore.load();
    },
});