    Ext.define('QL.Helper',{
        Date:function(){
            let tanggal = new Date();
            return `${tanggal.getDate()}/${tanggal.getMonth()+1}/${tanggal.getFullYear()}`;
        },
        formatUSD:function(value){
            Ext.util.Format.thousandSeparator=",";
            return Ext.util.Format.usMoney(value);
        },
        formatIDR:function(value){
            Ext.util.Format.thousandSeparator=".";
            return Ext.util.Format.currency(value,"Rp",".",""," ")
        },
        thousand:function(value){
           return (parseFloat(parseFloat(value).toFixed(2))).toLocaleString(undefined,{ minimumFractionDigits: 2 })
            // Ext.util.Format.thousandSeparator=".";
            // return Ext.util.Format.number(value,"0,000.00")
        }
    });