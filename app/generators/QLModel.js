
Ext.define('QLModel', {
    alias: 'controller.qlmodel',
    extend: 'Ext.app.ViewModel',
	data: {
		currentPage: 1
	},
	formulas: {
		currentPage: function(get) {
			return localStorage.pageId;
		}
	},
});