Ext.define('QL.DecoderV6',
function(){
    var self;
    return {
    name: 'QLDecoderV6',
    config : {
        type            : "header",
        data            : {},
        controller      : 'qlcontroller',
        formName        : null,
        rowSpaces       : [],
        indexJSON       : 0,
        title           : null,
        id              : null,
        rowSpaces       : [],
        newHbox         : [],
        isOnline        : false,
        cols            : 2,
        defaultWidth    : '100%',
        labelWidth      : '100%',
        convertedFields : [],
        properties      : [],
        buttons         : [],
        buttonAlign     : "center",
        grid            : null,
        margin          : "0 0 0 0"
    },
    constructor : function(config){
        self=this;
        self.initConfig(config);
        return self.get();
    },
    defaultSelect : (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                text: Ext.String.capitalize(arrayDefault[i]),
                value: arrayDefault[i]
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;
    
        return valuesAkhir;
    },
    defaultRadio : (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }            
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                label: Ext.String.capitalize(arrayDefault[i]),
                value: arrayDefault[i],
                ui:'solo'
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;
    
        return valuesAkhir;
    },
    defaultCheckBox : (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                label: Ext.String.capitalize(arrayDefault[i]),
                name: arrayDefault[i],
                value: arrayDefault[i],
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;        
        return valuesAkhir;
    },
    defaultCombo: (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                text: Ext.String.capitalize(arrayDefault[i]),
                id: arrayDefault[i],
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;        
        return valuesAkhir;
    },
    kamusElement:(column,isChild=false)=>{
        if(column['name']!=null && column['name']!=undefined){
            column.name=column.name.toLowerCase();
        }
        if(column.label==""||column.label==null||column.label==undefined){
            column.label="";
        }
        var defaultWidth = isChild?"100%":self.getDefaultWidth();
        if(column.withlabel != undefined && column.withlabel != null ){
            column.label = column.withlabel? column.label:"";
        }
        var convertMe = false;
        var elements;
        for(let index=0; index<self.getConvertedFields().length;index++){
            if( self.getConvertedFields()[index].id == column.id ){
                convertMe = true;
                elements = self.getConvertedFields()[index].fields;
                break;
            }
        }
        if(convertMe){
            return { 
                    xtype:'container',
                    autoSize:true,
                    cls:'qlfield',
                    // padding: "1 1 1 1",
                    margin: "1 30 4 5", //TOP RIGHT BOTTOM LEFT 
                    items:elements
            };
        }
        
        var data;
        if(["enum","select"].includes(column.type)){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data = {
                    name: column.name+"_id",
                    cls: "qlselectfield",
                    xtype: 'combobox',
                    editable:false,
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    autoSelect:true,
                    placeholder: "choose...",
                    bind : { value: `{${column.name+"_id"}}`}
                
            };
            if(column.value!=null){                    
                if( ((column.value).toLowerCase() ).includes("store:") ){
                    var store = (column.value.toLowerCase()).replace("store:","");
                    var storeArray = store.split(">");
                    var storeName = storeArray[0], storeId = storeArray[1], storeDisplay = storeArray[2];
                    Object.assign(data,{valueField:storeId,displayField:storeDisplay,store:storeName})
                }else{
                    data["options"] = self.defaultSelect(column.value);
                    data["value"] = data.options[0].value;
                }
            }
            // data["value"] = data.options[0].value;
        }else if(["enum_popup","popup"].includes(column.type)){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data ={
                    xtype: 'container',
                    cls: "qlpopup",
                    layout: 'hbox',
                    hidden: column.hidden,
                    label: column.label,
                    width: isChild?parseInt(self.tempWidth-80)+"%":defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelAlign : 'left',   
                    required:column.required,
                    labelWrap:true,
                    items: [{
                        xtype: 'textfield',
                        cls: "qltextfield",
                        readOnly:true,
                        disabled: column.disabled,
                        required:column.required,
                        name: column.name,
                        labelWidth: self.getLabelWidth(),
                        width: `${ parseInt( isChild?self.tempWidth: defaultWidth) -(isChild?0:17)}%`,
                        label: column.label,
                        labelAlign : 'left',    
                        labelWrap:true,
                        style:{
                            "margin-bottom": "0px !important"
                        },
                        bind : { value: `{${column.name}}`}
                    }, {
                        xtype: 'textfield',
                        hidden:true,
                        name: column.name+"_id",
                        bind : { value: `{${column.name+"_id"}}`}
                    }, 
                    {
                        xtype: 'button',
                        ui: 'confirm',
                        width:"33px",
                        iconCls: 'x-fa fa-search',
                        handler: 'formPopup',
                        cls: 'goes-btn--popup',
                        style: {
                            "margin-left":"3px"
                        },
                        popupStore: function(){
                            // if( ( (column.value).toLowerCase() ).includes("store:") ){
                            //     return column.value.replace("store:","");
                            // }else{
                                return "samplePopupStore"
                            // }
                        }
                    }, 
                    {
                        xtype: 'button',
                        ui: 'altdecline',
                        title: ' reset',
                        width:"33px",
                        iconCls: 'x-fa fa-lg fa-eraser',
                        handler: 'resetField',
                        cls: 'goes-btn--popup',
                        style: {
                            "margin-left":"3px"
                        }
                    }
                ]
                
            };
            
        }else if(["enum_search","search"].includes(column.type)){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data ={
                    name: column.name+"_id",
                    xtype: 'combobox',
                    cls: "qlcombofield", 
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    flex:1,
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'id',       
                    minChars:0,
                    anyMatch:true,   
                    autoSelect:true,
                    forceSelection:true,
                    placeholder: "search...",
                    bind : { value: `{${column.name+"_id"}}`}  
                    // store: self.defaultCombo(column.value)
                
            };
            if(column.value!=null){                    
                if( ((column.value).toLowerCase() ).includes("store:") ){
                    var store = (column.value.toLowerCase()).replace("store:","");
                    var storeArray = store.split(">");
                    var storeName = storeArray[0], storeId = storeArray[1], storeDisplay = storeArray[2];
                    Object.assign(data,{valueField:storeId,displayField:storeDisplay,store:storeName})
                }else{
                    data["options"] = self.defaultSelect(column.value);
                    data["value"] = data.options[0].value;
                }
            }
        }else if(["enum_radio","radio"].includes(column.type)){
            data ={
                xtype: 'radiogroup',
                cls: "qlradiogroupfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                cls: 'x-radio-group-alt',
                name: column.name,
                items: self.defaultRadio(column.value),
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                bind : { value: `{${column.name}}`}
            };
            // data["value"] = data.items[0].value;
        }else if(["enum_checkbox","checkbox","check one","checkone","array"].includes(column.type)){                
            data ={
                xtype: 'checkboxfield',
                cls: "qlcheckboxfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                cls: 'x-check-group-alt',
                name: column.name,
                items: self.defaultCheckBox(column.value),
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                bind : { value: `{${column.name}}`}
            };
            // data["items"] = data.items[0].value;
        }else if(["enum_checkbox","checkbox","check many","checkmany","array"].includes(column.type)){                
            data ={
                xtype: 'checkboxgroup',
                cls: "qlcheckboxgroupfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                cls: 'x-check-group-alt',
                name: column.name,
                items: self.defaultCheckBox(column.value),
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
                bind : { value: `{${column.name}}`}
            };
            // data["items"] = data.items[0].value;
        }else if(["check-labelright"].includes(column.type)){
            data={
                xtype : "container",
                hidden: column.hidden,
                width: isChild? self.tempWidth:defaultWidth,
                layout:"hbox",
                items : [{
                    xtype    : "checkboxfield",
                    style    : "margin-right:4px",
                    required : column.required,
                    readOnly : column.readonly,
                    disabled : column.disabled,
                    name     : column.name,
                    checked  : column.default=="true"||column.default===true?true:false,
                    bind : { value: `{${column.name}}`}
                },{
                    xtype:"component",
                    html:`<div style='padding-top:5px'>${column.label}</div>`
                }],
                cls: "qlchecklabelright",
            }
        }else if(["file","upload","image","filefield","file one","files many"].includes(column.type)){
            if(column.type=='image'){
                data = 
                    {
                        xtype:'container',
                        items:[
                            {
                                xtype: 'containerfield',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        cls: "qltextfield",
                                        readOnly:true,
                                        labelWidth: self.getLabelWidth(),
                                        width: `${ parseInt( (defaultWidth).replace("%","")) -10}%`,
                                        label: column.label,
                                        labelAlign : 'left',                    
                                        labelWrap:true,
                                        hidden: column.hidden,
                                        required: column.required
                                    },
                                    {
                                        name: column.name,            
                                        xtype: 'filebutton',
                                        label: column.label,
                                        labelAlign : 'left',                    
                                        readOnly: column.readonly,
                                        disabled: column.disabled,
                                        hidden: column.hidden,
                                        required:column.required,
                                        margin:"5 0 0 3",
                                        height:'100%',
                                        msgTarget: 'side',
                                        accept: 'image',
                                        text:'',
                                        iconCls:"fas fa-images",
                                        listeners:{ 
                                            change:function(el,val) {
                                                let name = el.getFiles()[0].name;
                                                // console.log(el.up("container").getItems());
                                                el.up("container").getItems().items[0].setValue(name);
                                                var canvas = document.getElementById(column.name);
                                                var ctx=canvas.getContext("2d");
                                                var maxW=canvas.width;
                                                var maxH=canvas.height;
                                                var img = new Image;
                                                img.onload = function() {
                                                        var iw=maxW;
                                                        var ih=maxW;
                                                        var scale=Math.min((maxW/iw),(maxH/ih));
                                                        var iwScaled=iw*scale;
                                                        var ihScaled=ih*scale;
                                                        // canvas.width=iwScaled;
                                                        // canvas.height=ihScaled;
                                                        ctx.drawImage(img,0,0,maxW,maxH);
                                                        // output.value = canvas.toDataURL("image/jpeg",0.5);
                                                }
                                                img.src = URL.createObjectURL(el.getFiles()[0]);
                                                canvas.style.display='block';
                                                el.up('container').up('container').getItems().items[1].setHidden(false);
                                            },
                                        }
                                    }
                                ],
                            },
                            {
                                xtype: 'panel',
                                label: column.label,
                                labelAlign : 'left',                        
                                width: isChild? self.tempWidth:defaultWidth,
                                labelWidth: self.getLabelWidth(),
                                labelWrap:true,
                                errorTarget: 'side',
                                autoComplete:false,
                                readOnly: column.readonly,
                                disabled: column.disabled,
                                hidden: true,
                                required:column.required,
                                hidden:true,
                                padding:'10 10 10 10',
                                html: `<canvas class='canvas-image' id="${column.name}" height="70" style="margin-left:30%;margin-right:30%;width:90%;display:none;"></canvas>`,
                                listeners:{
                                    added:  ( sender, container, index, eOpts ) =>{
                                        // console.log('awef');
                                    }
                                }
                            },
    
                        ]
                    }    
                
            }else{
                data ={
                    name: column.name,            
                    xtype: 'filefield',
                    cls: "qlfilefield",
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    multiple : (column.type).includes("many")?true:false,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    msgTarget: 'side',
                    allowBlank: false,
                    // anchor: '100%',
                    buttonText: 'Select File',
                    bind : { value: `{${column.name}}`}
                };
            }
        }else if(["money","numeric","number"].includes(column.type)){
            data ={
                name: column.name,            
                xtype: 'qlnumberfield',
                cls: "qlnumberfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                value:column.default,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                // validators: /^[0-9.]{1,}$/,
                textAlign:"right",
                minValue: 0,
                bind : { value: `{${column.name}}`}
            };
        }else if(["date","datetime"].includes(column.type)){
            data ={
                name: column.name,            
                xtype: 'datepickerfield',
                cls: "qldatepickerfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                dateFormat:'d/m/Y',
                value : column.value=="now"?new Date:null, 
                placeholder:'dd/mm/yyyy',
                validators: 'date',
                bind : { value: `{${column.name}}`}
                // maxValue: new Date()
            };
        }else if(["time"].includes(column.type)){
            data ={
                name: column.name,            
                xtype: 'timefield',
                cls: "qltimefield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                format:'H:i',
                bind : { value: `{${column.name}}`}
            };
        }else if(["rowspaces","space"].includes(column.type)){
            data ={
                xtype   : 'field',
                isField : false,
                isFormField : false,
                style   :{
                    color:"red"
                }
            };
        }else if(["text_only","label","label_only","label only","text only"].includes(column.type)){
            data ={
                cls: "qllabel",
                html   : `<span>${column.label}</span>`,
                style:{
                    "font-size":"14px",
                    "color": "#656f75"
                }
            };
        }else if(["textarea"].includes(column.type)){
            data ={           
                xtype: 'textareafield',
                cls: "qltextareafield",
                name:column.name,
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                bind : { value: `{${column.name}}`}
            };
        }else if(["button"].includes(column.type)){
            data ={  
                xtype: 'button',
                ui: 'confirm',
                width:"33px",
                iconCls: 'far fa-paper-plane',
                style: {
                    "margin-left":"3px",
                    "margin-top":"7px"
                }
            };
        }else if(["string","varchar","text","email"].includes(column.type)){
            if( (column.label.toLowerCase()).includes("email") ){
                data ={
                    xtype: 'emailfield',
                    cls: "qlemailfield",
                    name:column.name,
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    value:column.default,
                    errorTarget: 'side',
                    autoComplete:false,
                    validators:'email',
                    placeholder: 'email@domain.com',
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    bind : { value: `{${column.name}}`}
                };
            }else{
                data ={
                    xtype: 'textfield',
                    cls: "qltextfield",
                    name:column.name,
                    label: column.label,
                    labelAlign : 'left',
                    value:column.default,
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    bind : { value:  column.value !==null && (column.value).includes("{") && (column.value).includes("}")? `${column.value}`:`{${column.name}}`}
                };
            }
        }else if(["hidden"].includes(column.type)){
            data ={
                name: column.name, 
                xtype: 'textfield',
                hidden:true,
                bind : { value: `{${column.name}}`}};
        }else{
            data ={
                name: column.name, 
                xtype: 'textfield',
                cls: "qltextfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                bind : { value: `{${column.name}}`}
                
            };
        }
        data["ui"] = 'solo';
        var propsMe = false;
        var props;
        for(let index=0; index<self.getProperties().length;index++){
            if( self.getProperties()[index].id == column.id ){
                propsMe = true;
                props = self.getProperties()[index].props;
                break;
            }
        }
        if(propsMe){
            if( !["enum_popup","popup"].includes(column.type) ){
                if( !['panel','formpanel','container'].includes(data.xtype) ){
                    if(props['bind']!==undefined){
                        Object.assign(data['bind'],props['bind']);
                        delete props['bind'];
                    }
                    Object.assign(data,props)
                }else{
                    for( let i=0;i < data.items.length; i++ ){
                        if( !['button','component'].includes(data.items[i].xtype) ){
                            if(props['bind']!==undefined){
                                Object.assign(data.items[i]['bind'],props['bind']);
                                delete props['bind'];
                            }
                            Object.assign(data.items[i],props);
                        }
                    }
                }
            }else{
                if(["enum_popup","popup"].includes(column.type)){
                    Object.assign(data.items[0],props.textfield==undefined?{}:props.textfield);   
                    Object.assign(data.items[1],props.idfield==undefined?{}:props.idfield);                       
                    Object.assign(data.items[2],props.popupbutton==undefined?{}:props.popupbutton);                 
                    Object.assign(data.items[3],props.erasebutton==undefined?{}:props.erasebutton);
                    props.textfield=undefined;
                    props.popupbutton=undefined;
                    props.erasebutton=undefined;
                    Object.assign(data.items[0],props);
                    if(props.fullPopup!==undefined){
                        Object.assign(data, props.fullPopup);
                    }
                }
            }
        }
        var fixedItems = [data];
        if(Array.isArray(data)!==true && ["selectfield","combobox"].includes(data["xtype"]) ){
            fixedItems.push({
                name:column.name,
                xtype:"textfield",
                hidden:true,
                bind : { value: `{${column.name}}`},
                afterRender:function(){
                    this.setValue(this.up("formpanel").getValues()[column.name+"_id"]);
                }
            });
            var listener = data["listeners"]===undefined?{}:data["listeners"];
            Object.assign(listener, {                
                select:function(el,val){
                    var text={};
                    text[column.name]=val.data[el.getDisplayField()];
                    el.up("formpanel").setValues(text);
                }
            });
            data["listeners"]=listener;
        }
        if(["hidden"].includes(column.type)){
            return data;
        }
        return isChild?fixedItems:{
                xtype:'container',
                autoSize:true,
                cls:'qlfield',                
                margin: self.getMargin(), //TOP RIGHT BOTTOM LEFT 
                items:fixedItems
        };
    },
    get : ()=>{
        var data = self.getData().forms[self.getIndexJSON()];
        var jsonVersion = self.getData()['json_version']===undefined?1:self.getData()['json_version'];
        var semuaKolom = [];
        var columns = data.data;
        var colsTemp = columns;
        var columns  = [];
        var hiddenfields = [];
        var joining_id = null;
        for(let i=0; i<colsTemp.length; i++){
            if(jsonVersion==2){
                colsTemp[i]['label'] = (colsTemp[i]["label-name-type"]).split("*-*")[0];
                colsTemp[i]['name'] = (colsTemp[i]["label-name-type"]).split("*-*")[1];
                colsTemp[i]['type'] = (colsTemp[i]["label-name-type"]).split("*-*")[2];
                colsTemp[i]['isChild'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[0]=="true"?true:false;
                colsTemp[i]['required'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[1]=="true"?true:false;
                colsTemp[i]['readonly'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[2]=="true"?true:false;
                colsTemp[i]['disabled'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[3]=="true"?true:false;
                colsTemp[i]['hidden'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[4]=="true"?true:false;
                colsTemp[i]['withlabel'] = colsTemp[i]['withlabel']!==undefined?colsTemp[i]['withlabel']:true;
            }
           if(colsTemp[i]['isChild']){
                if(joining_id==null){
                    joining_id = i-1;
                    colsTemp[joining_id]['child']=[];
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }else{
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }
           }else{
               joining_id=null;
           }
        }
        columns = colsTemp.filter(function(data){
            if(data.type=="hidden"){
                hiddenfields.push(data);
            }
            if(data.isChild!==true && data.type!="hidden"){
                return data;
            }
        });

        var inlineRowSpaces = [];
        var hitung=0;
        for(let i=0;i<columns.length ;i++){
            for(let j=0;j<self.getRowSpaces().length;j++){
                if( self.getRowSpaces()[j].id == columns[i].id ){
                    for(let k=0;k<self.getRowSpaces()[j].total;k++){
                        inlineRowSpaces.push(i+( (self.getRowSpaces()[j].position=="before")?1:2)+(hitung++) );
                    }
                    break;
                }
            }
        }
        Ext.iterate(inlineRowSpaces,function(index){
            columns.splice( (index-1), 0, {
                "name": "rowspaces"+index,
                "type": "rowspaces",
                "label": "rowspaces",
            });  
        });
        if(columns.length%self.getCols() > 0){
            columns.push({
                "name": "rowspacesother",
                "type": "rowspaces",
                "label": "rowspaces",
            });
        }
        var jumlahBaris = Math.ceil( columns.length/self.getCols() );        

        for (let i = 0; i < jumlahBaris ; i++) {
            var hasilFields = [];
            if( columns[i].child == undefined ){
                hasil =  self.kamusElement(columns[i]);
            }else{
                var hasil = {
                    xtype   : 'containerfield',
                    cls     : "qlsandingan",
                    isField : false,
                    layout  : 'hbox',           
                    labelWrap:true,         
                    label  : columns[i].withlabel?columns[i].label:""
                }
                var items = [];
                let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[i].type=='label only'?0:1))-3}%`
                self.tempWidth=defaultWidth;
                
                if(!["label only"].includes(columns[i].type)){
                    columns[i].withlabel=false;
                    var childArr=self.kamusElement(columns[i],true);
                    items.push( Object.assign(
                        childArr[0],{
                            style:{
                                "margin-left":"0px",
                                "max-height": !["textarea","popup"].includes(columns[i].type)?"38px":"100%"
                            }
                        })
                    );
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                for(let j=0;j<columns[i].child.length;j++){
                    var childArr=self.kamusElement(columns[i].child[j],true);
                    var child=childArr[0];
                    if(!["label only"].includes(columns[i].type) || j>0){
                        Object.assign(child,{
                            style:{
                                "margin-left":"5px",
                                "max-height":!["textarea","popup"].includes(columns[i].child[j].type)?"38px":"100%"
                            }
                        });
                    }
                    items.push(child);
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                hasil['items']=items;
            }
            hasilFields.push(hasil);

            try{
                if( columns[jumlahBaris+i].child ==undefined ){
                    var hasil = self.kamusElement(columns[jumlahBaris+i]);
                }else{
                    var hasil = {
                        xtype   : 'containerfield',
                        cls     : "qlsandingan",
                        layout  : 'hbox',                    
                        labelWrap:true,
                        label  : columns[jumlahBaris+i].withlabel?columns[jumlahBaris+i].label:""
                    }
                    var items = [];
                    let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[jumlahBaris+i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[jumlahBaris+i].type=='label only'?0:1))-3}%`
                    self.tempWidth=defaultWidth;
                    
                    if(!["label only"].includes(columns[jumlahBaris+i].type)){
                        columns[jumlahBaris+i].withlabel=false;
                        var childArr=self.kamusElement(columns[jumlahBaris+i],true);
                        items.push( Object.assign(
                            childArr[0],{
                                style:{
                                    "margin-left":"0px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].type)?"38px":"100%"
                                }
                            })
                        );
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    for(let j=0;j<columns[jumlahBaris+i].child.length;j++){
                        var childArr=self.kamusElement(columns[jumlahBaris+i].child[j],true);
                        var child=childArr[0];
                        if(!["label only"].includes(columns[jumlahBaris+i].type) || j>0){
                            Object.assign(child,{
                                style:{
                                    "margin-left":"5px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].child[j].type)?"38px":"100%"
                                }
                            });
                        }
                        items.push(child);
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    hasil['items']=items;
                }
                hasil["style"]={ "margin-left" : "30px"}
                hasilFields.push( hasil );
            }catch(err){}

            semuaKolom.push( {
                defaults: {
                    xtype: 'textfield',
                    border: true,
                    width: 450,
                    cls: "qlfieldperbaris",
                    margin: "5 0 5 0"
                },
                // flex:1,
                layout: 'fit',
                items : hasilFields,
                autoSize:true,
            } );
        }
        hiddenfields.forEach(function(dt){
            semuaKolom.push( 
                self.kamusElement(dt) 
            );
        });
        console.log("%c Form: "+( self.getData().process ).replace(/ /g,"_")+" created "+(self.getIsOnline()?"[online] ":"[data]"),"background: green; color: red;font-weight: bold;");
        for(let i=0;i<self.getNewHbox().length;i++){
            // console.log(self.getNewHbox()[i]);
            semuaKolom.splice(self.getNewHbox()[i].index,0,self.getNewHbox()[i].container);
        }
        if(self.getGrid()!=null){
            var grid = self.getGrid();
            grid.columns.push({
                align:'center',
                text : "Remove",
                width:"10%",
                cell: {
                    tools: {
                        delete: {
                            iconCls: 'x-fa fa-trash',
                            handler: function(el,row){
                                row.grid.getStore().remove(row.record);
                            }
                        },
                    }
                }
            });
            Object.assign(grid,{
                xtype:"grid",
                rowLines: true, 
                columnLines: true, 
                rowNumbers: {
                    text:'No',
                    align:"center",
                    width: "5%"
                },
                columnResize:false,
                afterRender:function(){
                    var mygrid = this;
                    mygrid.getStore().on("datachanged",function(){
                        let datalength = mygrid.getStore().getData().length;
                        mygrid.setHeight(datalength*32+60);
                    },mygrid.getStore());
                }
            });
            return {
                xtype : "panel",
                bodyPadding : 0,
                autoSize:true,
                items:[
                    {
                        bodyPadding : 10,
                        xtype       :'formpanel',
                        title       : self.getTitle(),
                        id          : self.getId(),        
                        afterRender :function(){
                            this.setController(self.getController())
                        },
                        listeners: {
                            delegate    : 'field',
                            buffer      : 10, // buffer as validating each field may trigger an errorchange
                            errorchange : 'formFieldErrors'
                        },
                        items   : semuaKolom,
                        buttonAlign : self.getButtonAlign(),
                        buttons    : self.getButtons()
                    },
                    grid
                ]
            }
        }else{
            return {
                bodyPadding : 25,
                xtype       :'formpanel',
                title       : self.getTitle(),
                id          : self.getId(),
                afterRender :function(){
                    this.setController(self.getController())
                },
                listeners: {
                    delegate    : 'field',
                    buffer      : 10, // buffer as validating each field may trigger an errorchange
                    errorchange : 'formFieldErrors'
                },
                items   : semuaKolom,
                buttonAlign : self.getButtonAlign(),
                buttons    : self.getButtons()
            };
        };

    }
    
}});