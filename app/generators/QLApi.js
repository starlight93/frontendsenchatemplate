Ext.define('QL.Api', function(){
    var me;
    return {
    constructor:function(data={}){
        me = this;
        me.writerUrl = data.writerUrl;
        me.readerUrl = data.readerUrl;
        me.loginUrl  = data.loginUrl;
        me.project = data.project;
        me.rootProperty = data.rootProperty;
        let allStores = Ext.StoreManager.keys;
        allStores.forEach(function( storeName ){
            if( storeName.includes("store_online") ){
                var store = Ext.StoreManager.lookup(storeName);
                store.setProxy(
                    me.getData(store.api)
                );
                // store.load();
            }
            
        });
    },
    writerUrl   : "",
    readerUrl   : "",
    loginUrl    : "",
    token       : "",
    project     : "",
    rootProperty: "",
    headers     : {},
    setToken    : function(token){
        me.token = token;
    },
    setHeaders  : function(data){
        localStorage.auth=JSON.stringify(data);
        me.headers = data;
    },
    //================================================================
    changeStatus : function(data,callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url:`${me.readerUrl}/${data.model}/${data.id}`,
            headers: me.headers,
            method: "PATCH",
            jsonData :{status:data.status},
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
            },   
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
                Ext.Msg.alert("Failed!","Maybe Server Error");
            }
        })
    },
    // =======================================================
    getDraftId:function(callback=function(code){}){
        Ext.Ajax.request({
            url: `https://laradev2.dejozz.com/get-draft-id`,
            method:"GET",
            success: function(response, opts) {
                callback(response.responseText);
            },    
            failure: function(response, opts) {                
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    getArrayData:function(data={},callback=function(json){}, error=function(json){}){
        var params = data.params == undefined ? {}:data.params;
        let keys = Object.keys(params);
        var parameters="";
        keys.forEach(function(key){
            var value = params[key];
            parameters += `${key}=${value}&`
        });
        Ext.Ajax.request({
            headers: me.headers,
            url: `${me.readerUrl}/${data.model}?${parameters}`,
            method:"GET",
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
            },    
            failure: function(response, opts) {                
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
                Ext.Msg.alert("Failed!","Maybe Server Error");
                Ext.getCmp("loadingDialogBOM").hide();
            }
        });
    },
//===============================================================
    getData: function(data={},custom=false){
        var params = data.parameters == undefined ? {}:data.parameters;
        var url  = custom? `${qlconfig[`backend_${qlconfig.development?'development':'production'}_custom`]}/${data.model}/${data.function}`:`${me.readerUrl}/${data.model}`;
        var proxy=  {
                headers:me.headers,
                type: 'ajax',
                url: url,
                extraParams : params,
                // paramsAsJson:true,
                reader: {
                    type: 'json',
                    rootProperty: (data['rootProperty']!==undefined?data['rootProperty']:me.rootProperty),
                },
            }
        return proxy;
    },
//================================================================
    create : function(data,callback=function(json){}, error=function(json){}){
        let payloads = data.data;
        let keys = Object.keys(payloads)
        for(let i=0;i< keys.length;i++){
            if( !Array.isArray(payloads[keys[i]]) ){
                if( !(/^[a-z0-9_]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( Array.isArray(payloads[keys[i]]) && !(keys[i]).includes("_detail")){
                if( !(/^[a-z0-9_]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( (keys[i]).includes("_detail")){
                if(payloads[keys[i]].length>0){
                    var detailKeys= Object.keys(payloads[keys[i]][0]);
                    for(let j=0;j< detailKeys.length;j++){                    
                        if( !(/^[a-z0-9_]{1,}$/).test(detailKeys[j]) ){
                            Ext.Msg.alert("Correction!",`field [${detailKeys[j]}] di [${keys[i]}] tidak sesuai standar nama`);
                            return true;
                        }
                    }
                }
            }
        }
            
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: `${me.writerUrl}/${data.model}`,
            headers: me.headers,
            method: "POST",
            jsonData :data.data,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
                // console.dir(obj);
            },   
            failure: function(response, opts) {
                error(Ext.decode(response.responseText));
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
            }
        });
    },
    delete : function(data,callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: `${me.writerUrl}/${data.model}/${data.id}`,
            headers: me.headers,
            method: "DELETE",
            jsonData :data.data,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
                // console.dir(obj);
            },   
            failure: function(response, opts) {
                error(Ext.decode(response.responseText));
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
            }
        });
    },

    update : function(data,callback=function(json){}, error=function(json){}){
        let payloads = data.data;
        let keys = Object.keys(payloads)
        for(let i=0;i< keys.length;i++){
            if( !Array.isArray(payloads[keys[i]]) ){
                if( !(/^[a-z0-9_]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( Array.isArray(payloads[keys[i]]) && !(keys[i]).includes("_detail")){
                if( !(/^[a-z0-9_]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( (keys[i]).includes("_detail")){
                if(payloads[keys[i]].length>0){
                    var detailKeys= Object.keys(payloads[keys[i]][0]);
                    for(let j=0;j< detailKeys.length;j++){                    
                        if( !(/^[a-z0-9_]{1,}$/).test(detailKeys[j]) ){
                            Ext.Msg.alert("Correction!",`field [${detailKeys[j]}] di [${keys[i]}] tidak sesuai standar nama`);
                            return true;
                        }
                    }
                }
            }
        }
        
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: `${me.writerUrl}/${data.model}/${data.id}`,
            headers: me.headers,
            method: "PUT",
            jsonData :data.data,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
                // console.dir(obj);
            },   
            failure: function(response, opts) {
                error(Ext.decode(response.responseText));
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
            }
        });
    },

    read : function(data,callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url:`${me.readerUrl}/${data.model}/${data.id}`,
            headers: me.headers,
            method: "GET",
            jsonData :data.data,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
                // console.dir(obj);
            },   
            failure: function(response, opts) {
                error(Ext.decode(response.responseText));
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
            }
        })
    },

    login : function(data,callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").show();
            Ext.Ajax.request({
                url: me.loginUrl,
                method: "POST",
                jsonData :data.data,
                success: function(response, opts) {
                    me.afterLogin();
                    var obj = Ext.decode(response.responseText);
                    callback(obj);
                    Ext.getCmp("loadingDialog").hide();
                    console.log('request success');
                },   
                failure: function(response, opts) {
                    Ext.getCmp("loadingDialog").hide();
                    error(JSON.parse(response.responseText) );
                    var text = "Request Failed!"
                    if( [422,401].includes(response.status)){
                        text = "Username or Password was not correct!"
                    }else if( [405].includes(response.status)){
                        text = "Method Not Allowed"
                    }
                    Ext.Msg.alert("Failed!",text);
                }
        });
    },
    getMe : function(callback=function(json){}, error=function(json){}){
        if(!qlconfig.use_login){
            
            if(localStorage.first=='1'){
                if((Ext.Viewport.getItems().items).length>0){
                    Ext.Viewport.remove(0);
                }
                Ext.Viewport.add([{xtype:"Login"}]);
                me.afterLogin();
            }else{
                if((Ext.Viewport.getItems().items).length>0){
                    Ext.Viewport.remove(0);
                }
                Ext.Viewport.add([{xtype:"projectmainview"}]);                
                var oldXtype = localStorage.xtype!=undefined?localStorage.xtype:"Home";
                window.location.href="#-";
                setTimeout(function () {
                    window.location.href="#"+oldXtype;
                },100);
            }
            localStorage.first="0";
            return true;
        }
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url      :qlconfig[`backend_${qlconfig.development?'development':'production'}_auth`],
            headers  : me.headers,
            method   : "GET",
            // jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText);
                callback(obj);
                if(localStorage.first=='1'){
                    if((Ext.Viewport.getItems().items).length>0){
                        Ext.Viewport.remove(0);
                    }
                    Ext.Viewport.add([{xtype:"projectmainview"}]);
                    // Ext.Viewport.add([{xtype:localStorage.xtype}]);
                    var oldXtype = localStorage.xtype;
                    window.location.href="#-";
                    if( (window.location.href).includes("#")){
                        setTimeout(function () {
                            window.location.href="#"+oldXtype;
                        },100);
                    }
                    me.afterLogin();
                }
                localStorage.first="0";
            },   
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                console.log('server-side failure with status code ' + response.status);
                if([401].includes(response.status)){
                    if((Ext.Viewport.getItems().items).length>0){
                        Ext.Viewport.remove(0);
                    }
                    Ext.Viewport.add( {xtype:'Login'} );
                }
            }
        })
    },
    afterLogin: function(){
        let me = this;
        let allStores = Ext.StoreManager.keys;
        allStores.forEach(function( storeName ){
            if( storeName.includes("store_online") ){
                var store = Ext.StoreManager.lookup(storeName);
                store.setProxy(
                    me.getData(store.api)
                );
                // store.load();
            }
            
        });
    },
    
    custom : function(data,callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url:`${qlconfig[`backend_${qlconfig.development?'development':'production'}_custom`]}/${data.model}/${data.function}`,
            headers: me.headers,
            method: data['method']==undefined?'GET':data['method'],
            jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText);
                callback(obj);
            },   
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                error(JSON.parse(response.responseText) );
                console.log('server-side failure with status code ' + response.status);
            }
        })
    },
  }
//===================================================
});