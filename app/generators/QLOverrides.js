Ext.define('QL.exporterGrid', {
    override: 'Ext.exporter.Base',
    applyTitle: function(title) {
        return title ? 'EXCEL REPORT - ' + title : title;
    }
});
Ext.define("QL.overrideComponent",{
    override: "Ext.Component",
    
    beforeInitConfig: function(config) {
        if( this.xtype!==undefined && (this.xtype=='formpanel' || (this.xtype).includes("field")) ){
            if(document.getElementById(this.getId()) !==null ){
                Ext.getCmp(this.getId()).destroy();
            }
        }
        this.beforeInitialize.apply(this, arguments);
    },
});
Ext.define("QL.overrideFormPanel",{
    override:"Ext.form.Panel",
    reset: function(clearInvalid) {
        this.getFields(false).forEach(function(field) {
            field.reset();
 
            if( ['combobox','selectfield'].includes(field.xtype) ){
                field.clearValue();
                field.setInputValue(null);
            }
            if (clearInvalid) {
                field.setError(null);
            }
        });
 
        return this;
    },
    resetExcept: function(array) {
        this.getFields(false).forEach(function(field) {
            if( !array.includes(field.getName()) ){
                field.reset();
                if( ['combobox','selectfield'].includes(field.xtype) ){
                    field.clearValue();
                }
                field.setError(null);
            }
        });
 
        return this;
    },
    resetOnly: function(array) {
        this.getFields(false).forEach(function(field) {
            if( array.includes(field.getName()) ){
                field.reset();
                if( ['combobox','selectfield'].includes(field.xtype) ){
                    field.clearValue();
                }
                field.setError(null);
            }
        });
 
        return this;
    },    
    setReadOnly:function(isReadOnly=true){
        var fields = this.getFields();
        var keys = Object.keys(fields);
        keys.forEach(function(key){
            let field = fields[key];
            if(Array.isArray(field)){
                if(field.length>0){
                    if(field[0]['xtype']!==undefined && ["radiogroup"].includes(field[0].xtype)){
                        var radios = field[0].getItems().items;
                        radios.forEach(function(radio){
                            radio.setDisabled(true);
                        });
                    }
                }
            }else if(["checkboxfield","checkboxgroup"].includes(field["xtype"])){
                field.setDisabled(true);
            }else{
                field.setReadOnly(true);
            } 
        });
    },
     getValues: function(options, nameless) {
        //  if(this.validate()){
        //     Ext.Msg.alert("Form is not valid!","Check your form data!");
        //     return null;
        //  }
        var fields = this.getFields(),
            values = {},
            isArray = Ext.isArray,
            enabled = options,
            field, value, addValue, bucket, name, ln, i, serialize;
 
        if (Ext.isObject(options)) {
            enabled = options.enabled;
            nameless = options.nameless;
            serialize = options.serialize;
        }
 
        // Function which you give a field and a name, and it will add it into the values
        // object accordingly
        addValue = function(field, name) {
            if ((!nameless && (!name || name === 'null')) || field.isFile) {
                return;
            }
 
            // Checkboxes have a "value" but it is behaves differently than regular
            // fields. When the checkbox is checked, its value is returned and otherwise
            // it returns null. By default, that value is true. This is handled by
            // serialize/getSubmitValue so we always need to call it for checkboxes.
            // value = (serialize || field.isCheckbox) ? field.serialize() : field.getValue();
            if (serialize || field.isCheckbox) {
                value = field.serialize();
            } else if(typeof field.getDateFormat === "function" && field.getDateFormat()) {
                value = Ext.Date.format(field.getValue(), field.getDateFormat());
            }else if(field.isXType("timefield")){
                value = field.getInputValue()
            } else {
                value = field.getValue();
            }
            if (!(enabled && field.getDisabled())) {
                // RadioField is a special case where the value returned is the fields valUE
                // ONLY if it is checked
                if (field.isRadio) {
                    if (field.isChecked()) {
                        values[name] = value;
                    }
                }
                else {
                    // Check if the value already exists
                    bucket = values[name];
 
                    if (!Ext.isEmpty(bucket)) {
                        if (!field.isCheckbox || field.isChecked()) {
                            // if it does and it isn't an array, we need to make it into an array
                            // so we can push more
                            if (!isArray(bucket)) {
                                bucket = values[name] = [bucket];
                            }
 
                            // Check if it is an array
                            if (isArray(value)) {
                                // Concat it into the other values
                                bucket = values[name] = bucket.concat(value);
                            }
                            else {
                                // If it isn't an array, just pushed more values
                                bucket.push(value);
                            }
                        }
                    }
                    else {
                        values[name] = value;
                    }
                }
            }
        };
 
        // Loop through each of the fields, and add the values for those fields.
        for (name in fields) {
            if (fields.hasOwnProperty(name)) {
                field = fields[name];
 
                if (isArray(field)) {
                    ln = field.length;
 
                    for (i = 0; i < ln; i++) {
                        addValue(field[i], name);
                    }
                }
                else {
                    addValue(field, name);
                }
            }
        }
 
        return values;
    },
    /* @return {Ext.field.Manager} this
    */
    setValues: function(values) {
        var fields = this.getFields(),
            name, field, value, ln, i, f;

        values = values || {};
        for (name in values) {
            if (values.hasOwnProperty(name)) {
                field = fields[name];
                value = values[name];

                if (field) {
                    // If there are multiple fields with the same name. Checkboxes, radio
                    // fields and maybe event just normal fields..
                    if (Ext.isArray(field)) {
                        ln = field.length;

                        // Loop through each of the fields
                        for (i = 0; i < ln; i++) {
                            f = field[i];

                            if (f.isRadio) {
                                // If it is a radio field just use setGroupValue which
                                // will handle all of the radio fields
                                f.setGroupValue(value);
                                break;
                            }
                            else if (f.isCheckbox) {
                                if (Ext.isArray(value)) {
                                    f.setChecked(value.indexOf(f._value) !== -1);
                                }
                                else {
                                    f.setChecked(value === f._value);
                                }
                            }
                            else {
                                // If it is a bunch of fields with the same name, check
                                // if the value is also an array, so we can map it to
                                // each field
                                if (Ext.isArray(value)) {
                                    f.setValue(value[i]);
                                }
                            }
                        }
                    }
                    else {
                        if (field.isRadio || field.isCheckbox) {
                            // If the field is a radio or a checkbox
                            field.setChecked(value);
                        }
                        else {
                            // If just a normal field
                            if( (field.xtype).includes("combobox") ){
                                    if( field.getStore().loadCount<1 ){
                                        field.getStore().load();
                                    }
                            }
                            field.setValue(value);
                            if( (field.xtype).includes("number") ){
                                    field.onBlur();
                            }else if( (field.xtype).includes("textfield") && field['isChildCombo']===undefined ){
                                    field.onBlur();
                            }else if( (field.xtype).includes("combobox") ){
                                if(fields[ name.replace("_id","") ] !==undefined){
                                    fields[ name.replace("_id","") ].setValue(field.getInputValue());
                                }
                            }else{
                                if(field['isChildCombo']!==undefined && fields[field.getName()+"_id"]!==undefined && values[field.getName()+"_id"]===undefined){
                                    var name = field.getName();
                                    var parent = fields[name+"_id"];
                                    if(parent!==undefined){
                                        var key = parent.getDisplayField();
                                        if( parent.getStore().loadCount < 1 ){
                                            parent.getStore().setAsynchronousLoad(true);
                                            parent.getStore().setListeners({
                                                load:function(thisStore,thisRecords){
                                                        for(let y=0; y<thisRecords.length;y++){
                                                            if(thisRecords[y]['data'][key] == value){
                                                                parent.setValue( thisRecords[y]['data'][parent.getValueField() ] );
                                                                break;
                                                            };
                                                        }
                                                    }
                                            })
                                            parent.getStore().load();
                                            parent.getStore().setAsynchronousLoad(false);
                                            parent.getStore().setListeners({
                                                load: function(){}
                                            })
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (this.getTrackResetOnLoad && this.getTrackResetOnLoad()) {
                        field.resetOriginalValue();
                    }
                }
            }
        }
        
        return this;
    }
});
Ext.define('QL.checkboxField', {
    override: 'Ext.field.Checkbox',
    getSubmitValue: function() {
        return this.getChecked() ? Ext.isEmpty(this._value) ? true : this._value : false;
    },
});
Ext.define('QL.textfield',{
    override: 'Ext.field.Text',
    onKeyDown: function(event) {
        if (event.browserEvent.keyCode === 13) {
            event.preventDefault();
            return false;
        }
        var me = this,
            inputMask = me.getInputMask(); 
        me.lastKeyTime = Date.now(); 
        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }
        me.ignoreInput = true;
 
        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        } 
        me.fireAction('keydown', [me, event], 'doKeyDown');
    }
});
Ext.define('QL.numberfield', {
    extend: 'Ext.field.Text',
    xtype: 'qlnumberfield',
    clearable:false,
    isInput:false,
    textAlign:"right",
    decimals:2,
    autoComplete:false,
    alternateClassName: 'Ext.form.NumberNew',

    onFocusLeave: function(event) {
        this.isInput=false;
        var me = this,
            inputMask = me.getInputMask();

        me.callParent([event]);

        me.removeCls(me.focusedCls);
        me.syncLabelPlaceholder(true);

        if (inputMask) {
            inputMask.onBlur(me, me.getValue());
            console.log('ada input mask');
        }
        if(this.getInputValue()!==null && this.getInputValue()!=""&& this.getInputValue()!="."){
            let rawValue =this.getInputValue(); 
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString();
            // this.setValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
            this.setInputValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
        }
    },
    onBlur: function() {
        this.isInput=false;
        if(this.getInputValue()!==null && this.getInputValue()!=""&& this.getInputValue()!="."){
            let rawValue =this.getInputValue(); 
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString();
            // this.setValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
            this.setInputValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
        }
    },
    doKeyUp: function(me, e) {
        me.syncEmptyState();
        if(me.getInputValue()!==null && me.getInputValue()!=""&& me.getInputValue()!="."){
            let rawValue =me.getInputValue(); 
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"":(rawValue).split(".")[1];
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString();
            me.setInputValue(formattedVal+((rawValue).includes(".")?`.${decimal}`:""));
        }
        if (e.browserEvent.keyCode === 13) {
            me.fireAction('action', [me, e], 'doAction');
        }
    },
    
    onKeyDown: function(event) {
        var me = this,
            inputMask = me.getInputMask();
        this.isInput=true;
        me.lastKeyTime = Date.now();
 
        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }
 
        // tell the class to ignore the input event. this happens when we want to listen
        // to the field change when the input autocompletes
        me.ignoreInput = true; 
        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        }
 
        me.fireAction('keydown', [me, event], 'doKeyDown');
        let key = event.event.key;
        // console.log(event.event.key);
        if(key=="."){
            if( me.getValue()===null || (me.getValue()).length==0 || (me.getValue()).includes(".") ){
                event.preventDefault();
                return true;
            }
        }
        if( !(/^[0-9.]{1,}$/).test(key) 
            && !["ArrowLeft","ArrowRight","Backspace","Delete"].includes(key) 
            && !(event.event.ctrlKey===true && ["v","c","x","a"].includes(key)) ){

            event.preventDefault();
            return true;

        }
    },
    
    updateInputValue: function(value) {
        if(value===NaN){
            console.log('iya situ NaN');
        }
        var inputElement = this.inputElement.dom;
        if (inputElement.value !== value) {
            inputElement.value = value;
        }
        
        if(!this.isInput&&value!==null &&value!=""&& value!="."){
            var me = this;
            let rawValue =value; 
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString();
            me.setInputValue(formattedVal+((rawValue).includes(".")?`.${decimal}`:""));
            // inputElement.value=formattedVal+((rawValue).includes(".")?`.${decimal}`:"");
        }
    },
    updateValue: function(value, oldValue) {
        if (this.canSetInputValue()) {
            this.setInputValue(value);
        }
        this.callParent([value, value]);
    },
    
    applyValue: function(value) {
        if (this.isConfiguring) {
            this.originalValue = value;
        }
        return value;
    },
    
    applyInputValue: function(value) {
        try{
            this.setValue(value.replace(/,/g,""));
        }catch(e){}
        return (value != null) ? (value + '') : '';
    },

});
Ext.define('QL.textnumberfield', {
    extend: 'Ext.field.Text',
    xtype: 'qltextnumberfield',
    autoComplete:false,
    alternateClassName: 'Ext.form.TextNumberNew',
    allow:"",
    onKeyDown: function(event) {
        var me = this,
            inputMask = me.getInputMask();
        this.isInput=true;
        me.lastKeyTime = Date.now(); 
        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }
        me.ignoreInput = true; 
        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        }
        me.fireAction('keydown', [me, event], 'doKeyDown');
        let key = event.event.key;
        if( !(me.allow).includes(key)){
            if( !(/^[0-9]{1,}$/).test(key) 
                && !["ArrowLeft","ArrowRight","Backspace","Delete"].includes(key) 
                && !(event.event.ctrlKey===true && ["v","c","x","a"].includes(key)) ){
                event.preventDefault();
                return true;
            }   
        }
    }
});
Ext.define('QL.navigationView', {
    override: 'Ext.navigation.View',
    cls:"goes-content"
});

Ext.define('QL.overrideComboBox',{
    override: "Ext.field.ComboBox",
    updateStore: function(store, oldStore) {
        if(this.getItemTpl()!==undefined && !(this.getItemTpl()).includes(`<span class="x-list-label">`)){
            var newData = this.getItemTpl();
            store.getProxy().getReader().setTransform(function(data){
                
                var fixedData = data[store.getProxy().getReader().getRootProperty()];
                fixedData.forEach(function(perData,index){
                    let dataBaru=newData;
                    Object.keys( perData ).forEach(function(key){
                        dataBaru = (dataBaru).replace(`{${ key }}`, perData[key]);
                        fixedData[index]['itemTpl'] = dataBaru;
                    });
                });
                return fixedData;
            });
            this.setDisplayField("itemTpl")
        }
        var me = this,
            isRemote = me.getQueryMode() === 'remote',
            primaryFilter,
            proxy, oldFilters;
        if (isRemote) {
            store.setRemoteFilter(true);
            proxy = store.getProxy();
 
            if (proxy.setFilterParam) {
                proxy.setFilterParam(me.getQueryParam());
            }
        }
        me.callParent([store, oldStore]);
        primaryFilter = me.getPrimaryFilter();
 
        if (primaryFilter) {
            if (oldStore && !oldStore.destroyed) {
                oldFilters = oldStore.getFilters();
                if (oldFilters) {
                    oldFilters.remove(primaryFilter);
                }
            }
            me._pickerStore.addFilter(primaryFilter, true);
        }
        if (me.getQueryMode() === 'local') {
            store.on({
                filterchange: 'onStoreFilterChange',
                scope: me
            });
        }
    },
});