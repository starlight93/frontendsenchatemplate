var qlconfig = 

// =================================================WILAYAH PENGATURAN DIMULAI

{
    project                     : "Project - Enterprise",
    development                 : true,
    use_backend                 : false,
    use_login                   : false,
    //==============================================BACKEND CONFIGURATION
    backend_development_api     : "https://backend.dejozz.com/laradev2/public/public-resources/sampleproject",
    backend_development_custom  : "https://backend.dejozz.com/laradev2/public/public-resources/sampleproject",
    backend_development_login   : "https://backend.dejozz.com/laradev2/public/login",
    backend_development_auth    : "https://backend.dejozz.com/laradev2/public/me",
    backend_development_root_key: "data",
    backend_development_parameters:{
        paginate : "paginate",
        page     : "current_page",
        page_end : "last_page",
        filter_fields : "filterField",
        filter_text   : "search",
        order_by : "orderBy",
        order_type: "orderType",
        select_fields : "selectField"
    },

    backend_production_api      : "https://backend.dejozz.com/laradev2/public/public-resources/sampleproject",
    backend_production_custom   : "https://backend.dejozz.com/laradev2/public/public-resources/sampleproject",
    backend_production_login    : "https://backend.dejozz.com/laradev2/public/login",
    backend_production_auth     : "https://backend.dejozz.com/laradev2/public/me",
    backend_production_root_key : "data",
    backend_production_parameters:{
        paginate : "paginate",
        page     : "current_page",
        page_end : "last_page",
        filter_fields : "filterField",
        filter_text   : "search",
        order_by : "orderBy",
        order_type: "orderType",
        select_fields : "selectField"
    },

    backend_pdf_renderer        : "https://qqltech.com/git_pdf_renderer/one.php"
    //===============================================BACKEND CONFIGURATION END

    
}

//==================================================WILAYAH PENGATURAN SELESAI